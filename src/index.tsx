import { createRoot } from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "src/redux";

import App from "./components/App";

import "sweetalert2/src/sweetalert2.scss";
import "./index.scss";

const router = createBrowserRouter([
	{
		path: "*",
		element: <App />,
	},
]);
const container = document.getElementById("root");
const root = createRoot(container);

root.render(
	<Provider store={store}>
		<RouterProvider router={router} />
	</Provider>
);
