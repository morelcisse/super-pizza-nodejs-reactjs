import { PayloadAction, createSlice } from "@reduxjs/toolkit";

export interface ISettingsState {
	settings: any;
	loadingSettings: boolean;
}

const initialState: ISettingsState = {
	settings: {},
	loadingSettings: false,
};

const settingsSlice = createSlice({
	name: "settings",
	initialState,
	reducers: {
		setSettings: (state, action: PayloadAction<{ settings: any }>) => {
			const { settings } = action.payload;
			state.settings = settings;
			state.loadingSettings = false;
		},
		updateSettings: (state, action: PayloadAction<{ settings: any }>) => {
			const { settings } = action.payload;
			const s = action.payload.settings;
			const index = state.settings.findIndex((st) => st._id === s._id);
			state.settings[index] = { ...settings[index], ...s };
			state.loadingSettings = false;
		},
		setSettingsLoading: (state, action: PayloadAction<boolean>) => {
			state.loadingSettings = action.payload;
		},
	},
});

export const { setSettings, updateSettings, setSettingsLoading } = settingsSlice.actions;
export default settingsSlice;
