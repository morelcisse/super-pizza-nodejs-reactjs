import { combineReducers, configureStore, EmptyObject } from "@reduxjs/toolkit";
import { useDispatch as useDispatchBase, useSelector as useSelectorBase } from "react-redux";
import appSlice, { IAppState } from "src/redux/appReducer";
import cartSlice, { ICartState } from "src/redux/cartReducer";
import ordersSlice, { IOrdersState } from "src/redux/ordersReducer";
import pizzasSlice, { IPizzasState } from "src/redux/pizzasReducer";
import settingsSlice, { ISettingsState } from "src/redux/settingsReducer";
import usersSlice, { IUserState } from "src/redux/usersReducer";
import { PERSIST } from "redux-persist";
import persistReducer from "redux-persist/es/persistReducer";
import storage from "redux-persist/lib/storage";
import authSlice, { IAuthState } from "src/redux/authReducer";

const transform = {
	transforms: [],
};

const persistConfig = {
	...transform,
	key: "auth",
	storage,
};

const rootReducers = combineReducers({
	app: appSlice.reducer,
	users: usersSlice.reducer,
	orders: ordersSlice.reducer,
	cart: cartSlice.reducer,
	settings: settingsSlice.reducer,
	pizzas: pizzasSlice.reducer,
	auth: persistReducer(persistConfig, authSlice.reducer),
});

export const store = configureStore({
	reducer: rootReducers,
	middleware: (getDefaultMiddleware: any) => getDefaultMiddleware({ serializableCheck: { ignoredActions: [PERSIST] } }),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = EmptyObject & {
	app: IAppState;
	users: IUserState;
	orders: IOrdersState;
	cart: ICartState;
	settings: ISettingsState;
	pizzas: IPizzasState;
	auth: IAuthState;
};

export const useDispatch = () => useDispatchBase<AppDispatch>();
export const useSelector = <TSelected = unknown>(
	// eslint-disable-next-line no-unused-vars
	selector: (state: RootState) => TSelected
): TSelected => useSelectorBase<RootState, TSelected>(selector);
