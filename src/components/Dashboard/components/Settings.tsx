import { ChangeEventHandler, useEffect, useState } from "react";
import Swal from "sweetalert2";

import Modal from "src/components/Modal/Modal";
import axiosInstance from "src/helpers/Axios";
import { settingsTrans } from "src/helpers/Helpers";
import Loading from "src/components/Loading/Loading";

import { useDispatch, useSelector } from "src/redux";
import { setSettings, setSettingsLoading, updateSettings } from "src/redux/settingsReducer";

function Settings() {
	const [isOpen, setIsOpen] = useState(false);
	const loading = useSelector(({ settings }) => settings.loadingSettings);
	const settings = useSelector(({ settings }) => settings.settings);
	const [setting, setSetting] = useState({});
	const [values, setValues] = useState({});
	const dispatch = useDispatch();

	const handleSetting = (s: {}) => {
		setSetting(s);
		setValues(s);
	};

	const handleChange: ChangeEventHandler<HTMLInputElement> = (evt) => {
		const { value } = evt.target;
		const { name } = evt.target;

		setValues({ [name]: value });
	};

	const handleUpdate = async () => {
		dispatch(setSettingsLoading(true));

		const s = { ...setting, ...values };
		const response = await axiosInstance().put(`/settings/update/${s["_id"]}`, { setting: s });
		const { data } = response;
		const { message } = data;

		if (message === "Setting updated") {
			dispatch(updateSettings({ settings: s }));
			Swal.fire({
				title: "Succès",
				text: "Les paramètres ont bien été modifiée avec succès.",
				icon: "success",
				confirmButtonText: "OK",
			}).then((res) => {
				if (res.isConfirmed) {
					setIsOpen(false);
					setValues({});
					setSetting({});
				}
			});
		}
	};

	const getSettings = () => async () => {
		dispatch(setSettingsLoading(true));
		await axiosInstance()
			.get("/settings")
			.then(({ data }) => {
				const { settings } = data;
				dispatch(setSettings({ settings }));
			});
	};

	useEffect(() => {
		getSettings();
	}, []);

	if (loading) return <Loading />;

	return (
		<div className="container">
			<div className="table-responsive">
				<table className="table table-striped table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Nom</th>
							<th>Valeur</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						{settings.map((_setting, i) => {
							const _id = _setting[Object.keys(_setting)[0]];
							const key = Object.keys(_setting)[1];
							const value = _setting[key];

							return (
								<tr key={_id}>
									<td className="table-primary">
										<div>{i + 1}</div>
									</td>
									<td>
										<div>{settingsTrans[key]}</div>
									</td>
									<td>
										<div>{value}</div>
									</td>
									<td className="table-warning">
										<div className="btn-group" role="group" aria-label="Basic example">
											<button
												onClick={() => {
													handleSetting({ _id, name: key, [key]: value });
												}}
												data-bs-toggle="modal"
												data-bs-target="#settingsModal"
												type="button"
												className="btn btn-secondary">
												Modifier
											</button>
										</div>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>

				<Modal
					isOpen={isOpen}
					toggle={() => setIsOpen(!isOpen)}
					handleClose={() => setIsOpen(false)}
					title="Modifier les paramètres"
					buttonName="Modifier"
					handleValid={handleUpdate}>
					<div className="input-group mt-4 mb-4">
						<input
							value={values["order_capacity_by_hour"] || ""}
							onChange={handleChange}
							type="number"
							name="order_capacity_by_hour"
							aria-label="Capacité de commande par heure"
							placeholder="Capacité de commande par heure"
							className="form-control"
						/>
					</div>
				</Modal>
			</div>
		</div>
	);
}

export default Settings;
